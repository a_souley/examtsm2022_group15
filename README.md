This Python project is done in collaboration by

Group 15
1. Akshata Kudtarkar
2. Boukari Amadou

Brief descrption of the project:
RESTATURANT SIMULATION

1.	PART 1
	
	Python file - ExploratoryData.py;
	Libraries used- pandas,
					pandas_profiling,
					matplotlib;
	Results generated- rapportPart1.html,
						AvgCostBarPlot.png,
						TotalCost_byTIME_BarPlot.png,
						AvgCost_byTIME_BarPlot.png,
						CoursesHistDistr.png,
						AvgCostBarPlot.png,
						
					
2.	PART 2
	
	Python file - Clustering.py;
	Libraries used- pandas,
					numpy,
					matplotlib;
	Results generated- KmeansScatterPlt.png,
						LabelsToCsv.csv;
						
						
3.	PART 3
	
	Python file - Distribution.py;
	Libraries used- pandas,
					numpy,
					matplotlib;
	Results generated- Allocation_cluster_pie.png,
						likelihood_clients_cources.png,
						Drinks_hist_distrib.png.
	