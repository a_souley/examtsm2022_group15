import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
from sklearn.cluster import KMeans

plt.style.use('ggplot')

inputs1 = "../Data/Part3.csv"
inputs2 = "../Results/LabelsToCsv.csv"
inputs3 = "../Data/Part1.csv"

outputs = "../Results/"

Data3 = pd.read_csv(os.path.abspath(inputs1), sep=",")
MyDataFrame1 = pd.read_csv(os.path.abspath(inputs2), sep=",")

print(Data3.head())
print(MyDataFrame1.head())

## PART 3 QUESTION 2
# Grouping by the new column variables to have an overview
# Checking for allocation in each cluster
df = MyDataFrame1.groupby(["TIME", 'clusters']).size()
df.to_csv(os.path.abspath('../Results/distribution_of_clients .csv'), sep=',')


print("________________________________________________________________________________________________\n"
      "This is the detailed allocation between groups\n"
      "________________________________________________________________________________________________\n",df)

# Plotting the pie chart for overall allocation in each cluster
df2 = MyDataFrame1.groupby(['clusters']).size()
df2.columns = ['a','b']
print("________________________________________________________________________________________________\n"
      "This is the overall allocation between groups\n"
      "________________________________________________________________________________________________\n",df2)
plt.figure(figsize=(16,8))
# plot chart
ax1 = plt.subplot(121, aspect='equal')
df2.plot(kind='pie', y = 'cluster', ax=ax1, autopct='%1.1f%%',
 startangle=90, shadow=False, legend = False, fontsize=14)
plt.savefig(os.path.abspath('../Results/Allocation_cluster_pie.png'))
#plt.show()

print("________________________________________________________________________________________________\n"
      "This is the output of the original dataset added with the Labels\n"
      "________________________________________________________________________________________________\n",
      MyDataFrame1)

# Lets substract costumers per cluster in case we want to use it to propose a promotion
BusinessGroup = MyDataFrame1[MyDataFrame1["clusters"] == 0]
RetirementGroup = MyDataFrame1[MyDataFrame1["clusters"] == 1]
HealthyGroup = MyDataFrame1[MyDataFrame1["clusters"] == 2]
NormalGroup = MyDataFrame1[MyDataFrame1["clusters"] == 3]

print("________________________________________________________________________________________________\n"
      "This is an example of Retirement costumers group \n"
      "________________________________________________________________________________________________\n",
      RetirementGroup)
print(MyDataFrame1)


# Using lambda function to create a column with the corresponding client types

MyDataFrame1['client_type'] = MyDataFrame1.clusters.map( lambda x: "Business" if x == 0
else "Retirement" if x == 1 else "Healthy" if x == 2 else "Normal")
print(MyDataFrame1)

# Starters:  Soup $3, Tomato-Mozarella $15, Oysters $20
# Mains: Salad $9, Spaghetti $20, Steak $25, Lobster $40
# Desserts: Ice cream $15, Pie $10


# part 3 - section 2
# Adding the counts for number of times the type of course was selected
MyDataFrame1.loc[MyDataFrame1['FIRST_COURSE'] == 0, 'FIRST_COURSE_COUNT'] = 0
MyDataFrame1.loc[MyDataFrame1['FIRST_COURSE'] > 0, 'FIRST_COURSE_COUNT'] = 1
MyDataFrame1.loc[MyDataFrame1['SECOND_COURSE'] == 0, 'SECOND_COURSE_COUNT'] = 0
MyDataFrame1.loc[MyDataFrame1['SECOND_COURSE'] > 0, 'SECOND_COURSE_COUNT'] = 1
MyDataFrame1.loc[MyDataFrame1['THIRD_COURSE'] == 0, 'THIRD_COURSE_COUNT'] = 0
MyDataFrame1.loc[MyDataFrame1['THIRD_COURSE'] > 0, 'THIRD_COURSE_COUNT'] = 1
print(MyDataFrame1)

df3 = MyDataFrame1.groupby('clusters').sum()
#print(df3)

result = pd.concat([df3, df2], axis=1, join="inner")
result.columns = ['CLIENT_ID', 'FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE','FIRST_COURSE_COUNT', 'SECOND_COURSE_COUNT', 'THIRD_COURSE_COUNT', 'Total']
result.drop(['CLIENT_ID', 'FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE'], axis=1, inplace=True)
print(result)
ax = result.plot.bar(rot=0)

# Plotting a bar chart for likelihood of each type of clients choosing a specific course meal
plt.savefig(os.path.abspath('../Results/likelihood_clients_cources.png'))
#plt.show()



## PART3 SESSION 3
###  probabilities?
# Being a Business and getting:
ProbBnsStarter =  MyDataFrame1[(MyDataFrame1.client_type == "Business") & (MyDataFrame1.FIRST_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Business"].count()["client_type"]
ProbBnsMain =  MyDataFrame1[(MyDataFrame1.client_type == "Business") & (MyDataFrame1.SECOND_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Business"].count()["client_type"]
ProbBnsDessert =  MyDataFrame1[(MyDataFrame1.client_type == "Business") & (MyDataFrame1.THIRD_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Business"].count()["client_type"]


# Being Retirement and getting:
ProbRmtStarter =  MyDataFrame1[(MyDataFrame1.client_type == "Retirement") & (MyDataFrame1.FIRST_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Retirement"].count()["client_type"]
ProbRmtMain = MyDataFrame1[(MyDataFrame1.client_type == "Retirement") & (MyDataFrame1.SECOND_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Retirement"].count()["client_type"]
ProbRmtDessert =  MyDataFrame1[(MyDataFrame1.client_type == "Retirement") & (MyDataFrame1.THIRD_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Retirement"].count()["client_type"]


# Being Healthy and getting:
ProbHealthyStarter =  MyDataFrame1[(MyDataFrame1.client_type == "Healthy") & (MyDataFrame1.FIRST_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Healthy"].count()["client_type"]
ProbHealthyMain = MyDataFrame1[(MyDataFrame1.client_type == "Healthy") & (MyDataFrame1.SECOND_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Healthy"].count()["client_type"]
ProbHealthyDessert = MyDataFrame1[(MyDataFrame1.client_type == "Healthy") & (MyDataFrame1.THIRD_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Healthy"].count()["client_type"]


# Being Normal and getting:
ProbNormalStarter =  MyDataFrame1[(MyDataFrame1.client_type == "Normal") & (MyDataFrame1.FIRST_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Normal"].count()["client_type"]
ProbNormalMain =  MyDataFrame1[(MyDataFrame1.client_type == "Normal") & (MyDataFrame1.SECOND_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Normal"].count()["client_type"]
ProbNormalDessert =  MyDataFrame1[(MyDataFrame1.client_type == "Normal") & (MyDataFrame1.THIRD_COURSE != 0)].count()["client_type"] / MyDataFrame1[MyDataFrame1['client_type'] == "Normal"].count()["client_type"]

# creating dataframe for probabilities
df12 = pd.DataFrame({"BUSINESS":[ProbBnsStarter,ProbBnsMain,ProbBnsDessert],
                     "RETIREMENT":[ProbRmtStarter, ProbRmtMain, ProbRmtDessert],
                     "HEALTHY":[ProbHealthyStarter, ProbHealthyMain, ProbHealthyDessert],
                     "NORMAL":[ProbNormalStarter, ProbNormalMain, ProbNormalDessert]},
                    index = ["Starter", "Main", "Dessert"])

# Creating a csv fie for the table
df12.to_csv(os.path.abspath('../Results/ProbabilityOfGettingCourse.csv'), sep=',')

print(df12)

# part 3 - section 5

# Finding cost of Drinks per client to later chart distribution
MyDataFrame2 = pd.read_csv(os.path.abspath(inputs3), sep=",")

# Menu prices per course
Course1MenuPrices = [3, 15, 20]
Course2MenuPrices = [9, 20, 25, 40]
Course3MenuPrices = [10, 15]

# Finding the nearest food and getting the cost of drink
def Drinks1(x):
    drink = 0
    for i in Course1MenuPrices:
        if x > i:
            drink = x - i
    return drink

def Drinks2(x):
    drink = 0
    for i in Course2MenuPrices:
        if x > i:
            drink = x - i
    return drink

def Drinks3(x):
    drink = 0
    for i in Course3MenuPrices:
        if x > i:
            drink = x - i
    return drink

print(MyDataFrame2)

# Creating columns for each course cost of drinks

MyDataFrame2["Course1Drinks"] = MyDataFrame2["FIRST_COURSE"].map(Drinks1)
MyDataFrame2["Course2Drinks"] = MyDataFrame2["SECOND_COURSE"].map(Drinks2)
MyDataFrame2["Course3Drinks"] = MyDataFrame2["THIRD_COURSE"].map(Drinks3)
print("________________________________________________________________________________________________\n"
        "This is the table for each course cost of drinks\n"
        "________________________________________________________________________________________________\n",
             MyDataFrame2)

# plotting the cost distribution in a png file in results folder

MyDataFrame2.drop(['CLIENT_ID', 'TIME', 'FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE'], axis=1, inplace=True)
print(MyDataFrame2)
MyDataFrame2.plot.hist(subplots=True, layout=(3, 2), figsize=(20.0, 18.0), grid=True)
plt.suptitle('Courses costs distribution per type')
plt.savefig(os.path.abspath('../Results/Drinks_hist_distrib.png'))
# plt.show()