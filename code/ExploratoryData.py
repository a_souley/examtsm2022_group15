import pandas as pd
import os
from pandas_profiling import ProfileReport
import matplotlib.pyplot as plt

plt.style.use('ggplot')

inputs1 = "../Data/Part1.csv"
outputs = "../Results/"

MyDataFrame1 = pd.read_csv(os.path.abspath(inputs1), sep=",")

# Have a look of columns and their types

print(MyDataFrame1.columns)
print(MyDataFrame1.dtypes)

# Checking first and last rows to have an idea of the size

print(MyDataFrame1.head())
print(MyDataFrame1.tail())

# Getting few statistics by describing the dataset

print(MyDataFrame1.describe())

# Analysing with pandas profiling to have an overview on the dataset
# The outputs html files in Results should be opened with a browser

prof = ProfileReport(MyDataFrame1, title="Part1's file analysing")
prof.to_file(os.path.abspath('../Results/rapportPart1.html'))

"""
if the profiling doesn't work, follow the following steps:

Edit the file "~/[your_conda_env_path]/lib/site-packages/visions/dtypes/boolean.py"
Find the row "from pandas.core.dtypes.generic import ABCIndexClass, ABCSeries" and just replace ABCIndexClass for ABCIndex.
Save the boolean.py file and enjoy your report!
"""

# bar plotting the average cost per course in a png picture


plt.suptitle('average cost per course bar plotting')
plt.savefig(os.path.abspath('../Results/AvgCostBarPlot.png'))
# plt.show()

# bar plotting the Total cost per TIME, per course in a png picture

MyDataFrame1.groupby('TIME').sum().plot.bar()
plt.suptitle('Total cost per course by TIME bar plotting')
plt.savefig(os.path.abspath('../Results/TotalCost_byTIME_BarPlot.png'))
# plt.show()

# bar plotting the Average cost per TIME, per course in a png picture
MyDataFrame1.groupby('TIME').mean().plot.bar()
plt.suptitle('average cost per course by TIME bar plotting')
plt.savefig(os.path.abspath('../Results/AvgCost_byTIME_BarPlot.png'))
# plt.show()


# plotting the cost distribution in a png file in results folder

MyDataFrame1.plot.hist(subplots=True, layout=(3, 2), figsize=(20.0, 18.0), grid=True)
plt.suptitle('Courses costs distribution per type')
plt.savefig(os.path.abspath('../Results/CoursesHistDistr.png'))
# plt.show()


# bar plotting the average cost per course in a png picture

MyDataFrame1.mean().plot.bar()
plt.suptitle('average cost per course bar plotting')
plt.savefig(os.path.abspath('../Results/AvgCostBarPlot.png'))
# plt.show()

# Menu prices per course
Course1MenuPrices = [3, 15, 20]
Course2MenuPrices = [9, 20, 25, 40]
Course3MenuPrices = [10, 15]

# Finding the nearest food and getting the cost of drink
def Drinks1(x):
    drink = 0
    for i in Course1MenuPrices:
        if x > i:
            drink = x - i
    return drink

def Drinks2(x):
    drink = 0
    for i in Course2MenuPrices:
        if x > i:
            drink = x - i
    return drink

def Drinks3(x):
    drink = 0
    for i in Course3MenuPrices:
        if x > i:
            drink = x - i
    return drink
print(MyDataFrame1)

# Creating columns for each course cost of drinks

MyDataFrame1["Course1Drinks"] = MyDataFrame1["FIRST_COURSE"].map(Drinks1)
MyDataFrame1["Course2Drinks"] = MyDataFrame1["SECOND_COURSE"].map(Drinks2)
MyDataFrame1["Course3Drinks"] = MyDataFrame1["THIRD_COURSE"].map(Drinks3)
print("________________________________________________________________________________________________\n"
      "This is the table for each course cost of drinks\n"
      "________________________________________________________________________________________________\n",MyDataFrame1)

# Creating columns for each course cost of food
MyDataFrame1["Course1Food"] = MyDataFrame1["FIRST_COURSE"] - MyDataFrame1["Course1Drinks"]
MyDataFrame1["Course2Food"] = MyDataFrame1["SECOND_COURSE"] - MyDataFrame1["Course2Drinks"]
MyDataFrame1["Course3Food"] = MyDataFrame1["THIRD_COURSE"] - MyDataFrame1["Course3Drinks"]
print("________________________________________________________________________________________________\n"
      "This is the table for each course cost of food\n"
      "________________________________________________________________________________________________\n",MyDataFrame1)
#

