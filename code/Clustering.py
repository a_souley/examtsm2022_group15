import matplotlib.pyplot as plt
import pandas as pd
import os
import numpy as np
from sklearn.cluster import KMeans
import plotly.express as px
from mpl_toolkits.mplot3d import Axes3D



plt.style.use('ggplot')

inputs1 = "../Data/Part1.csv"
outputs = "../Results/"

MyDataFrame1 = pd.read_csv(os.path.abspath(inputs1), sep=",")
df = MyDataFrame1[["FIRST_COURSE", "SECOND_COURSE", "THIRD_COURSE"]]
print(MyDataFrame1)
print(df)

kmeans = KMeans(
    init="random",
    n_clusters=4,
    n_init=10,
    max_iter=300,
    random_state=42
)

kmeans.fit(df)

# print(kmeans.labels_)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# setting colors and scatter plotting the results

colormap = np.array(['Red', 'green', 'blue', "yellow"])
ax.scatter(df.FIRST_COURSE, df.SECOND_COURSE, df.THIRD_COURSE, c=colormap[kmeans.labels_])
plt.suptitle('Kmeans clustering\n'
             'according to the graph, we can state below groups of customers:\n'
             'Red: Business (cluster 1),\n'
             'Green: Retirement (cluster 2), \n '
             'Blue: Healthy (cluster 3),\n'
             'Yellow: Normal (cluster 4)'
             )

ax.set_xlabel("FIRST_COURSE")
ax.set_ylabel("SECOND_COURSE")
ax.set_zlabel("THIRD_COURSE")

plt.savefig(os.path.abspath('../Results/KmeansScatterPlt.png'))# plt.show()


# Adding labels to the dataset

predict = kmeans.predict(df)
MyDataFrame1['clusters'] = pd.Series(predict, index=df.index)
MyDataFrame1.to_csv(os.path.abspath('../Results/LabelsToCsv.csv'), sep=',')

