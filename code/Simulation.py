import pandas as pd
import random
import os

# Assuming 20 clients per day for 5 years * 365 days each year = total 36500 rows
def goToRestaurant(V_CUSTOMERTYPE1):
    if V_CUSTOMERTYPE1 == 'Business':
        return "Oysters", "Lobster", "Ice Cream", 1.4, 2.3, 1.6, 21.4, 42.3, 16.6

    if V_CUSTOMERTYPE1 == 'Retirement':
        return "Tomato-Mozarella", "Spaghetti", "Pie", 1.6, 0, 0, 4.6, 20, 10

    if V_CUSTOMERTYPE1 == 'Healthy':
        return "Soup", "Salad", "", 2.3, 0, 0, 5.3, 9, 0

    if V_CUSTOMERTYPE1 == 'Normal':
        return "", "Steak", "", 1.9, 0, 0, 1.9, 25, 0


rows = []
for i in range(36500):
    CUSTOMERTYPE_LIST = ['Business', 'Retirement', 'Healthy', 'Normal']
    TIME_LIST = ['LUNCH','DINNER']
    V_CUSTOMERTYPE = random.choice(CUSTOMERTYPE_LIST)
    V_TIME = random.choice(TIME_LIST)

    if V_CUSTOMERTYPE == 'Business':
            V_COURSE1, V_COURSE2, V_COURSE3, V_DRINKS1, V_DRINKS2, V_DRINKS3, V_TOTAL1, V_TOTAL2, V_TOTAL3 = goToRestaurant(V_CUSTOMERTYPE)

    if V_CUSTOMERTYPE == 'Retirement':
            V_COURSE1, V_COURSE2, V_COURSE3, V_DRINKS1, V_DRINKS2, V_DRINKS3, V_TOTAL1, V_TOTAL2, V_TOTAL3 = goToRestaurant(V_CUSTOMERTYPE)

    if V_CUSTOMERTYPE == 'Healthy':
            V_COURSE1, V_COURSE2, V_COURSE3, V_DRINKS1, V_DRINKS2, V_DRINKS3, V_TOTAL1, V_TOTAL2, V_TOTAL3 = goToRestaurant(V_CUSTOMERTYPE)

    if V_CUSTOMERTYPE == 'Normal':
            V_COURSE1, V_COURSE2, V_COURSE3, V_DRINKS1, V_DRINKS2, V_DRINKS3, V_TOTAL1, V_TOTAL2, V_TOTAL3 = goToRestaurant(V_CUSTOMERTYPE)

    V_CUSTOMERID = 'ID' + str(i)
    rows.append([V_TIME, V_CUSTOMERID, V_CUSTOMERTYPE, V_COURSE1, V_COURSE2, V_COURSE3, V_DRINKS1, V_DRINKS2, V_DRINKS3, V_TOTAL1, V_TOTAL2, V_TOTAL3])

df = pd.DataFrame(rows,columns=["TIME", "CUSTOMERID", "CUSTOMERTYPE", "COURSE1", "COURSE2", "COURSE3", "DRINKS1", "DRINKS2", "DRINKS3", "TOTAL1", "TOTAL2", "TOTAL3"])
# Creating a csv fie for the table
df.to_csv(os.path.abspath('../Results/Simulation_database.csv'), sep=',')
print(df)